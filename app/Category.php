<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    function bouquet(){
        return $this->belongsTo('App\Bouquet');
    }
}
