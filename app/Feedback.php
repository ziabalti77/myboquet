<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    //
    function user(){
       return $this->belongsTo('App\User');
    }

    function bouquet(){
        return $this->belongsTo('App/Bouquet');
    }
}
