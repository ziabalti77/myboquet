<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
    function create(Request $request){
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return redirect('/admin/dashboard/categories');
    }

    function delete(Request $request){
        $category = Category::find($request->id);
        $category->delete();
        return redirect('/admin/dashboard/categories');
    }

    function update(Request $request){
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->save();
        return redirect('/admin/dashboard/categories');
    }
}
