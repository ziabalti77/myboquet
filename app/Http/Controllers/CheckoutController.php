<?php

namespace App\Http\Controllers;

use App\Checkout;
use App\CustomCheckout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Adapter\Local;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class CheckoutController extends Controller
{
    function create(Request $request){

        if(Auth::user()){
            $amount = \Cart::session(Auth::user()->id)->getTotal();
        }


       Stripe::setApiKey('sk_test_qLEAAIPq5U5wCP00FLJffGqw');

        $customer = Customer::create(array(
            'email' => $request->stripeEmail,
            'source'  => $request->stripeToken
        ));
//        $card = $stripe->cards()->create($customer->id, $request->stripeToken); // add this to add a card.


        $charge = Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $request->amount,
            'currency' => 'usd'
        ));
        $productId=1;
        if(Auth::user()){
            $items = \Cart::session(Auth::user()->id)->getContent();
           foreach($items as $item){
               $checkout = new Checkout();
                $checkout->name = $request->name;
                $checkout->email = $request->email;
                $checkout->contact = $request->contact;
                $checkout->shippingaddress = $request->shippingaddress;
                $checkout->amount = $item->price;
                $checkout->quantity = $item->quantity;
                $checkout->bouquet_id = $item->id;
               $productId = $item->id;

                $checkout->save();
           }

           \Cart::session(Auth::user()->id)->clear();
        }else{
            $items = \Cart::session(Session::get('user_id'))->getContent();

            foreach($items as $item){
                $checkout = new Checkout();
                $checkout->name = $request->name;
                $checkout->email = $request->email;
                $checkout->contact = $request->contact;
                $checkout->shippingaddress = $request->shippingaddress;
                $checkout->amount = $item->price;
                $checkout->quantity = $item->quantity;
                $checkout->bouquet_id = $item->id;
                $checkout->save();
            }
            \Cart::session(Session::get('user_id'))->clear();
        }
//        return view('feedback', compact('productId'));
        return redirect()->route('getFeedback', ['productId' => $productId]);
//        return redirect('/bouquets/cart');
    }

    function customizeCheckout(Request $request){

//        dd($request);
        if(Auth::user()){
            Stripe::setApiKey('sk_test_RSnYuL3AluLYun2fMnEGIIiE');

            $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source'  => $request->stripeToken
            ));

            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $request->amount,
                'currency' => 'usd'
            ));
            $cc = new CustomCheckout();
            $cc->customername = $request->name;
            $cc->email = $request->email;
            $cc->contact = $request->contact;
            $cc->amountpaid = $request->amount;
            $cc->shippingAddress = $request->shippingaddress;
            $cc->wrapper = $request->wrapper_value;
            $cc->save();
            $productId =env('CUSTOM_PRODUCT_ID');
            return redirect()->route('getFeedback', ['productId' => $productId]);

        }
    }

    function getFeedback($productId){
     return view('feedback',compact('productId'));
    }


}
