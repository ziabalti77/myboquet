<?php

namespace App\Http\Controllers;

use App\Checkout;
use App\CustomCheckout;
use Illuminate\Http\Request;
use App\User;


class CustomerController extends Controller
{
    //
    function getAllCustomers(Request $request){
        $users = Checkout::all();
        $customOrderUsers = CustomCheckout::all();
        $all = array($users,$customOrderUsers);
//        dd($all);
        return view('admin.customers', compact('users','all'));
    }

    function deleteCustomer(Request $request) {
//        dd($request);
        $customer = Checkout::find($request->id);
        $customer->delete();
        return redirect('/admin/panel/customers/all');
    }
    function getAllUsers(Request $request){
        $users = User::all();
        return view('admin.allUsers', compact('users'));
    }
    function ordersToShip(){
        $orders = Checkout::all()->where('order_status_id', 1);
        return view('admin.ordersToShip', compact('orders'));
    }
}
