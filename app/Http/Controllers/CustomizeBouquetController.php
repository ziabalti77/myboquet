<?php

namespace App\Http\Controllers;

use App\Flower;
use Illuminate\Http\Request;

class CustomizeBouquetController extends Controller
{
    function index(){
        $flowers = Flower::all();
        return view('customize',compact('flowers'));
    }

    function delivery(Request $request){
        $count = $request->itemCount;
        $flowerObj = array();
        for($i=0;$i<$count;$i++){
            $item_name = "flower_key".$i;
            $item_qty = "flower_qty".$i;
            $item_id = "flower_id".$i;
            $flower = Flower::find($request->$item_id);
            $flower->qty = $request->$item_qty;
//            array_push($flowerObj,$request->$item_id);
            array_push($flowerObj,$flower);
//            array_push($flowerObj,$request->$item_qty);
        }
//        dd($flowerObj);
        return view('customDelivery', compact('flowerObj'));
    }
}
