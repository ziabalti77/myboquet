<?php

namespace App\Http\Controllers;

use App\Bouquet;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    function landing(){
        if(!Auth::user()){
            if(!Session::exists('user_id')){
                $user_id = rand();
                Session::put('user_id',$user_id);
            }

        }
        $categories = Category::all();
        $bouquets = Bouquet::all();
        $categoryName = "All Bouquets";
        return view('welcome',compact('categories','bouquets','categoryName'));
    }
}
