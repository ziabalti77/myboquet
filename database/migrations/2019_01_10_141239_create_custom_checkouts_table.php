<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customername');
            $table->text('email');
            $table->integer('contact');
            $table->text('shippingAddress');
            $table->integer('amountpaid');
            $table->string('wrapper');
            $table->integer('order_status_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_checkouts');
    }
}
