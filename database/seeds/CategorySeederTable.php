<?php

use Illuminate\Database\Seeder;

class CategorySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                'name' => 'Hand tied'
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Head tied'
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Bridal'
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Basket'
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Pomander'
            ]
        );

        DB::table('categories')->insert(
            [
                'name' => 'Single Stem'
            ]
        );
    }
}
