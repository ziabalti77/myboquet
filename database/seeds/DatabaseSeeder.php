<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FlowersTableSeeder::class);
        $this->call(CategorySeederTable::class);
        $this->call(BouquetTableSeeder::class);
        $this->call(OrderStatusTableSeeder::class);
        $this->call(WrapperTableSeeder::class);
    }
}
