<?php

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert(
            [
                'status' => 'Pending'
            ]
        );

        DB::table('order_statuses')->insert(
            [
                'status' => 'Dispatched'
            ]
        );
        DB::table('order_statuses')->insert(
            [
                'status' => 'Delivered'
            ]
        );
    }
}
