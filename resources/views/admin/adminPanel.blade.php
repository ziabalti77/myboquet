@extends('layouts.admin')
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <h1 class=" mb-4 ">General Information</h1>
                    <div class="card-deck">
                        <div class="card bg-primary">
                            <div class="card-body text-center">
                                <i class="fas fa-user fa-10x" style="color:white;"></i>
                                <h2 style=" margin-top:5px;color: white">{{$totalCustomers}} Customers</h2>
                            </div>
                        </div>
                        <div class="card bg-warning">
                            <div class="card-body text-center">
                                <i class="fas fa-cart-plus fa-10x" style="color:white;"></i>
                                <h2 style=" margin-top:5px;color: white">{{$orders}} Orders</h2>
                            </div>
                        </div>
                        <div class="card bg-success">
                            <div class="card-body text-center">
                                <i class="fas fa-object-group fa-10x" style="color:white;"></i>
                                <h2 style=" margin-top:5px;color: white">{{$items}} Items</h2>
                            </div>
                        </div>
                        <div class="card bg-danger">
                            <div class="card-body text-center">
                                <i class="fas fa-user-circle fa-10x" style="color:white;"></i>
                                <h2 style=" margin-top:5px;color: white">{{$registeredUsers}} Users</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 ">
            <div class="col-md-12">
                <h1>Admin Actions</h1>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-md-12">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Add Items</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Delete Items</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Update Items</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> View Customers</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> View Orders</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Manage Orders</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> View Registered Users</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Get Reports</h3></li>    <li class="list-group-item">
                        <span><i class="fa fa-check-circle fa-2x" style="color: green"></i></span>
                        <h3 style="display: inline-block; margin-left: 5px"> Manage Product Stock</h3></li>
                </ul>
            </div>
        </div>
    </div>
    @endsection