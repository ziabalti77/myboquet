@extends('layouts.admin')
@section('content')
<div class="wrapper mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-12 text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header">
                                <form action="{{route('addCategories')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <input class="form-control" placeholder="Enter Bouquet Category" id="category_name"  type="text" name="name"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <input class="btn btn-success form-control" type="submit" id="addCategory" value="Add" disabled/>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col" class="text-center">Name</th>
                                        <th scope="col" class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <th scope="row">{{$category->id}}</th>
                                            <td class="text-center">{{$category->name}}</td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a class="btn btn-success form-control" href="#" data-id="{{$category->id}}" id="updateCategory" data-name="{{$category->name}}" data-toggle="modal" data-target="#updateCategoryModal">Update</a>
                                                    <a class="btn btn-danger form-control" href="#" data-id="{{$category->id}}" id="deleteCategory" data-toggle="modal" data-target="#deleteCategoryModal">Delete</a>
                                                </div>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <!-- Delete Model -->
    <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="deleteCategoryModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this category?</p>
                    <form action="{{route('deleteCategory')}}" method="post">
                        {{csrf_field()}}
                        <input type="number" id="category_id" name="id" value="" hidden/>

                        <div class="row justify-content-end">
                            <div class="col-lg-2">
                                <input type="button" class="btn btn-primary" value="Cancel" data-dismiss="modal"/>
                            </div>
                            <div class="col-lg-4">
                                <input type="submit" class="form-control btn btn-danger" value="Delete">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <!-- Update modal -->
    <div class="modal fade" id="updateCategoryModal" tabindex="-1" role="dialog" aria-labelledby="updateCategoryModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('updateCategory')}}" method="post">
                        {{csrf_field()}}
                        <input type="number" id="category_id" value="{{$category->id}}" name="id" hidden>
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-primary" value="update">
                        </div>

                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", "#deleteCategory", function () {
            var id = $(this).data('id');
            $(".modal-body #category_id").val( id );
            // As pointed out in comments,
            // it is superfluous to have to manually call the modal.
            // $('#addBookDialog').modal('show');
        });

        // $(document).on("click", "#updateCategory", function () {
        //     var id = $(this).data('id');
        //     var name = $(this).data('name');
        //     $(".modal-body #category_id").val( id );
        //     $(".modal-body #name").val(name);
        //     // As pointed out in comments,
        //     // it is superfluous to have to manually call the modal.
        //     // $('#addBookDialog').modal('show');
        // });
        $(document).ready(function() {
            $('#category_name').on('input', function() {
                var nFilled = $('#category_name').filter(function() {
                    return $.trim( this.value ) !== '';
                }).length;
                $('#addCategory').prop('disabled', nFilled === 0);
            })
                .trigger('input');
        });
    </script>
</div>
    @endsection