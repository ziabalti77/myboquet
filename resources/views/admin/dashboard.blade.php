@include('layouts.admin')
<div class="wrapper mt-5">
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center"> Add Items</h4>
                </div>
                <div class="card-body">
                    <a href="{{route('admin.dashboard')}}" class="btn btn-primary btn-block">
                        Bouquets
                    </a>
                    <a href="{{route('admin.flowers')}}" class="btn btn-outline-primary btn-block">
                        Flowers
                    </a>
                    <a href="{{route('admin.wrappers')}}" class="btn btn-outline-primary btn-block">
                        Flower Wrappers
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    <div class="btn btn-primary btn-block float-right" data-toggle="modal" data-target="#addbouquet">Add Bouquet+</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($bouquets as $bouquet)

                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" style="height: 15rem !important;" src="/images/bouquets/{{$bouquet->bouquetImage}}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">{{$bouquet->name}}</h5>
                                    <p class="card-text">{{$bouquet->description}}</p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <form action="{{route('deleteBouquet')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="number" value="{{$bouquet->id}}" name="id" hidden/>
                                    <input class="btn btn-danger btn-block" type="submit" value="Delete"/>
                                </form>

                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="addbouquet" tabindex="-1" role="dialog" aria-labelledby="addbouquetModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('addBouquet')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Bouquet Name</label>
                                <input type="text" class="form-control" id="name" name="name"  placeholder="Enter bouquet name">
                            </div>
                            <div class="form-group">
                                <label for="categories">Category</label>
                                <select class="form-control" name="category" id="categories">
                                    <option value="1">Hand tied</option>
                                    <option value="2">Head tied</option>
                                    <option value="3">Bridal</option>
                                    <option value="4">Basket</option>
                                    <option value="5">Pomander</option>
                                    <option value="6">Single Stem</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control" id="description" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" name="price" id="price">
                            </div>
                            <div class="form-group">
                                <label for="price">Quantity</label>
                                <input type="number" class="form-control" name="quantity" id="quantity">
                            </div>
                            <div class="form-group">
                                <input type="file" name="bouquetImage">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Add Bouquet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>