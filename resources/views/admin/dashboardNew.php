<div class="container mt-4">
    <div class="row bg-info">
        <div class="col-md-12 text-center">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">ADD PRODUCT</a>
                </li>
                <li class="nav-item dropdown text-white">
                    <a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" href="#">ORDERS</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Pending Orders</a>
                        <a class="dropdown-item" href="#">Delivered Orders</a>
                        <a class="dropdown-item" href="#">Dispatched Orders</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">PRODUCT STOCK</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">CUSTOMERS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">USERS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">MANAGE CATEGORIES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">REPORTS</a>
                </li>
            </ul>
        </div>
    </div>

</div>