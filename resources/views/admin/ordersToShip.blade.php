@extends('layouts.admin')
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Orders for shipment</h1>
            </div>
        </div>
        <div class="container">
            <table class="table table-bordered mt-5">
                <thead class=" bg-dark text-white">
                <tr>
                    <th scope="col">Customer Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Address</th>
                    {{--<th scope="col">Amount</th>--}}
                    {{--<th scope="col">Qty</th>--}}
                    {{--<th scope="col">Order Status</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->name}}</td>
                        <td>{{$order->email}}</td>
                        <td>{{$order->contact}}</td>
                        {{--<td>{{$order->amount}}</td>--}}
                        <td>{{$order->shippingaddress}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection