@extends('layouts.admin')
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Order Reports</h1>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Most Ordered Product</h3>
                </div>
            </div>
            <table class="table table-bordered mt-2">
                <thead class=" bg-dark text-white">
                <tr>
                    <th>Image</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Category</th>
                    {{--<th scope="col">Ordered By</th>--}}
                    {{--<th scope="col">Address</th>--}}
                    {{--<th scope="col">Amount</th>--}}
                    {{--<th scope="col">Qty</th>--}}
                    {{--<th scope="col">Order Status</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($mo as $m)
                    <tr>
                        <td><img src="/images/bouquets/{{$m->bouquetImage}}" height="100px" width="100px"></td>
                        <td>{{$m->name}}</td>
                        <td>{{\App\Category::find($m->category_id)->name}}</td>
                        {{--<td>{{\App\Checkout::find($m->category_id)->name}}</td>--}}
                        {{--<td>{{$order->amount}}</td>--}}
                        {{--<td>{{$order->shippingaddress}}</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>Highest Rated Products</h3>
                <div class="container">
                    <table class="table table-bordered mt-2">
                        <thead class=" bg-dark text-white">
                        <tr>
                            <th>Image</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Ratings</th>
                            {{--<th scope="col">Times Sold</th>--}}
                            {{--<th scope="col">Ordered By</th>--}}
                            {{--<th scope="col">Address</th>--}}
                            {{--<th scope="col">Amount</th>--}}
                            {{--<th scope="col">Qty</th>--}}
                            {{--<th scope="col">Order Status</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cb as $c)
                            @if($c->product_id != -1)
                            <tr>
                                <td><img src="/images/bouquets/{{\App\Bouquet::find($c->product_id)->bouquetImage}}" height="100px" width="100px"></td>
                                <td>{{\App\Bouquet::find($c->product_id)->name}}</td>
                                <td>{{$c->rating}} STAR</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>Recurring Customers</h3>
                <div class="container">
                    <table class="table table-bordered mt-2">
                        <thead class=" bg-dark text-white">
                        <tr>
                            <th scope="col"> Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Joined On</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rb as $r)
                                <tr>
                                    <td>{{$r->name}}</td>
                                    <td>{{$r->email}}</td>
                                    <td>{{$r->created_at}} </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <hr>
    </div>
@endsection