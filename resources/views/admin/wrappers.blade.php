@extends('layouts.admin')
@section('content')
    <div class="wrapper mt-5">
        <div class="row">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center">Dashboard</h4>
                    </div>
                    <div class="card-body">
                        <a href="{{route('admin.dashboard')}}" class="btn btn-outline-primary btn-block">
                            Bouquets
                        </a>
                        <a href="{{route('admin.flowers')}}" class="btn btn-outline-primary btn-block">
                            flowers
                        </a>
                        <a href="{{route('admin.wrappers')}}" class="btn btn-primary btn-block">
                            Flower Wrappers
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-header">
                        <div class="btn btn-primary btn-block float-right" data-toggle="modal" data-target="#addbouquet">Add Wrapper+</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach($wrappers as $flower)
                                <div class="col-md-4">
                                    <div class="card">
                                        <img class="card-img-top" style="height: 12rem !important;" src="/images/wrappers/{{$flower->wrapperImage}}" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$flower->name}}</h5>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <form action="{{route('deleteWrapper')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="number" value="{{$flower->id}}" name="id" hidden/>
                                            <input class="btn btn-danger btn-block" type="submit" value="Delete"/>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="addbouquet" tabindex="-1" role="dialog" aria-labelledby="addbouquetModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Wrapper</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('addWrapper')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="name">Wrapper Name</label>
                                    <input type="text" class="form-control" id="name" name="name"  placeholder="Enter wrapper name">
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="price">Price</label>--}}
                                    {{--<input type="text" class="form-control" name="price" id="price">--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="price">Quantity</label>
                                    <input type="number" class="form-control" name="quantity" id="quantity">
                                </div>
                                <div class="form-group">
                                    <input type="file" name="wrapperImage">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Add Wrapper</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection