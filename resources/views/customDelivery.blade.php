@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-4">

        <div class="card">
            <div class="card-header">
                <h5 class="text-center">
                    Your Selected Flowers
                </h5>
            </div>
            <div class="card-body">
                <table class="table">

                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Total</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $grantTotal = array();
                    $i = 0;
                    $no = 1;
                    $sum = 0;
                    $totalqty = 0;

                    ?>
                    @foreach($flowerObj as $item)
                        <tr>
                            <th scope="row">
                                <img src="/images/flowers/{{$item->flowerImage}}" width="200px" height="200px"
                                     alt="flower image">
                            </th>
                            <td>{{$item->name}}</td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->qty}}</td>
                            <td>{{$grantTotal[$i] = $item->qty*$item->price}}</td>
                            {{--<td>--}}
                            {{--<form action="{{route('deleteCartItem',$item->id)}}" method="post">--}}
                            {{--{{csrf_field()}}--}}
                            {{--<input class="btn btn-danger" type="submit" value="delete"/>--}}
                            {{--</form>--}}

                            {{--</td>--}}
                        </tr>
                        <?php $i = $i + 1;$no++; $totalqty = $totalqty + $item->qty ?>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col col-lg-3"></div>
                    <div class="col col-lg-6">
                        <h1 class="text-center btn btn-info btn-block">
                            <div hidden>
                                @foreach($grantTotal as $total)
                                    {{$sum = $sum + $total}}
                                @endforeach
                            </div>
                            Total = {{$sum}}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2 mb-10">
            <div class="card-header">
                <h5 class="text-center">
                    Check Out
                </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-3"></div>
                    <div class="col-6">
                        <form action="{{route('createCustomCheckout')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="wrapper_value" id="wrapper_value"  required>
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="name" class="form-control" id="name" name="name"
                                       value="{{Auth::user() ? Auth::user()->name : ""}}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address:</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="{{Auth::user() ? Auth::user()->email : ""}}">
                            </div>
                            <div class="form-group">
                                <label for="contact">Contact#:</label>
                                <input type="number" class="form-control" id="contact" name="contact">
                            </div>

                            <div class="form-group">
                                <label for="amount">Amount:</label>
                                @if(isset($sum))
                                    <input type="number" class="form-control" id="amount" name="amount"
                                           value="{{$sum }}">
                                @else
                                    <input type="number" class="form-control" id="amount" name="amount" value="0">
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="shippingaddress">Shipping Address</label>
                                <textarea class="form-control" id="shippingaddress" name="shippingaddress"
                                          rows="4"></textarea>
                            </div>
                            <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="pk_test_8vpGSZGNXu2URmXCmucJb6cK"
                                    data-amount={{$sum}}
                                            data-name="myBouquet"
                                    data-description="Widget"
                                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                    data-locale="auto">
                            </script>
                        </form>
                    </div>
                    <div class="col-3">
                        <div>
                            <button class="btn btn-block btn-dark btn-block" data-target="#wrapperModal"
                                    data-toggle="modal">Add a Wrapper
                            </button>
                            <div class="mt-2">
                                <img src="" alt="" id="selectedWrapper" class="img-fluid">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Wrapper Modal -->
    <div class="modal fade" id="wrapperModal" tabindex="-1" role="dialog" aria-labelledby="wrapperModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Wrapper</h5>
                    <button type="button" class="close" id="closeModal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group wrapperError" id="error" >
                        <div class="alert alert-danger">
                            Select image first
                        </div>
                    </div>
                    <div class="form-group col-lg-8 mx-auto">
                        <img src="/images/default/wrapper_default.png" alt="" class="img-fluid" id="wrapperImage">
                    </div>
                    <div class="form-group">
                        <label for="name">Select Wrapper</label>
                        <select name="wrapper" id="selectWrapper" class="form-control">
                            <option value='null'>Select Wrapper</option>
                            @foreach(\App\Wrapper::all() as $wrapper)
                                <option value="{{$wrapper->wrapperImage}}">{{$wrapper->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary btn-block" id="wrapper_save_btn">Save</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#selectWrapper').on('change', function(){
            $('#error').addClass('wrapperError');
            if($(this).val() !== 'null'){
                $('#wrapperImage').attr('src',"/images/wrappers/"+$(this).val());
            } else{
                $('#wrapperImage').attr('src',"/images/default/wrapper_default.png");
            }
        });

        $('#wrapper_save_btn').on('click', function(){
            $('#error').addClass('wrapperError');
            if($('#selectWrapper').val() !== 'null'){
                $('#wrapper_value').attr('value',$('#selectWrapper').val());
                $('#selectedWrapper').attr('src',"/images/wrappers/"+$('#selectWrapper').val());
                $('#closeModal').click();
            }else {
                $('#error').removeClass('wrapperError');

            }
        })
    </script>
@endsection
