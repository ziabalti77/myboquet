@extends('layouts.app')
@section('content')
<div class="container-fluid">
    @if(isset($success))
        <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
            {{$success}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6 mt-4">
            <div class="card">
                <div class="card-body">
                    <img class="img-fluid" style="width: 80%;" src="/images/bouquets/{{$bouquet->bouquetImage}}" alt="Bouquet Image">
                </div>
            </div>
        </div>
        <div class="col-lg-6 mt-4">
            <div class="card-header bg-primary">
                {{$bouquet->name}}
            </div>
            <div class="card">
                <div class="card-body">
                    <hr>
                    <h4 class="text-info">price : ${{$bouquet->price}}</h4>
                    <hr>
                    <div class="card-text">
                        {{$bouquet->description}}
                    </div>
                </div>
            </div>
            <a class="btn btn-outline-success btn-block mt-4" href="{{route('cart',$bouquet->id)}}">Add To Cart</a>

        </a>
    </div>
</div>
    <div class="container ">
        <div class="row mt-4  bg-success rounded">
            <div class="col-md-6 text-left text-white font-weight-bold">
                <h1 class="mt-1">Reviews and Ratings</h1>
            </div>
            <div class="col-md-6 text-right">
                <button type="button" class="btn btn-outline-light mt-2 mr-7">
                    @if($feedbackScore > 0 )
                    Overall ratings  <span class="badge badge-light">{{$feedbackScore}}</span>
                        @else
                        Overall ratings  <span class="badge badge-light">Not Rated Yet</span>

                    @endif
                </button>            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-2">
                    @if($feedback->count() > 0)
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($feedback as $fb)
                                        <tr>
                                            <td><img height="50px" width="50px" src="https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png"
                                                     class="rounded-circle"><div class=" badge-success" style="width: 7rem"><h4 class="mt-2 ml-2">{{App\User::find($fb->user_id)->name}}</h4></div></td>
                                            <td >{{$fb->comment}}</td>
                                            <td><div class="badge badge-primary">{{date('m/d/y', strtotime($fb->created_at)) }}</div></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                    @else
                        <h4>No Reviews and Rating available</h4>
                        @endif
            </div>
        </div>
    </div>
</div>
@endsection