@extends('layouts.app')
@section('content')
    <div class="container mt-4 mtb-4">
        <div class="row">
        <div class="col-md-12 text-center">
            <h1>Feedback </h1>

            <p style="padding: 0.5rem">
          You feedback is very important for us to improve our services.
            </p>
        </div>
        </div>
        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <section class='rating-widget'>

                    <!-- Rating Stars Box -->
                    <div class='rating-stars text-center'>
                        <ul id='stars'>
                            <li class='star' title='Poor' data-value='1'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Fair' data-value='2'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Good' data-value='3'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Excellent' data-value='4'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='WOW!!!' data-value='5'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                        </ul>
                    </div>

                </section>
                <form action="{{route('createFeedback')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <textarea rows="6" cols="20" name="comment" class="form-control" placeholder="We really appreciate your comment!"></textarea>
                        <input type="number" id="rating" name="rating" hidden>
                        <input type="number" id="productId" name="productId" value="{{$productId }}" hidden>
                        <input type="number" id="userId" name="userId" value="{{Auth::user()->id}}" hidden>
                    </div>
                    <div class="form-group text-right">
                        <input type="submit" class=" btn btn-success">
                    </div>
                </form>
            </div>
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){

            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e){
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function(){
                $(this).parent().children('li.star').each(function(e){
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function(){
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                console.log('++++++++++++++RATING++++++++++++++');
                console.log(ratingValue)
                $('#rating').val(ratingValue)
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                }
                else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                responseMessage(msg);

            });


        });


        function responseMessage(msg) {
            $('.success-box').fadeIn(200);
            $('.success-box div.text-message').html("<span>" + msg + "</span>");
        }
    </script>
@endsection