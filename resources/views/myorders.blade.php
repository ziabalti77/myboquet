@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="mb-4 mt-2">Boucket Orders</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead class=" bg-primary text-white">
                    <tr>
                        {{--<th scope="col">#</th>--}}
                        <th scope="col">Order Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Amount Paid</th>
                        <th scope="col">Ordered Date</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($orders->count()>0)
                        @foreach($orders as $order)
                            <tr>
                                {{--<th scope="row">1</th>--}}
                                <td>{{\App\Bouquet::find($order->bouquet_id)->name}}</td>
                                <td>{{$order->shippingaddress}}</td>
                                <td>{{$order->amount}}</td>
                                <td>{{$order->created_at}}</td>
                                <td>{{\App\OrderStatus::find($order->order_status_id)->status}}</td>
                            </tr>
                        @endforeach
                    @else
                        <p>You have not placed any order yet!</p>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="mb-4 mt-2">Customized Bouquet Orders</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead class=" bg-primary text-white">
                    <tr>
                        {{--<th scope="col">#</th>--}}
                        <th scope="col">Customer Name</th>
                        <th scope="col">Shipping Address</th>
                        <th scope="col">Amount Paid</th>
                        <th scope="col">Ordered date</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($customizedorders->count()>0)
                        @foreach($customizedorders as $or)
                            <tr>
                                {{--<th scope="row">1</th>--}}
                                <td>{{$or->customername}}</td>
                                <td>{{$or->shippingAddress}}</td>
                                <td>{{$or->amountpaid}}</td>
                                <td>{{$or->created_at}}</td>
                                <td>{{\App\OrderStatus::find($or->order_status_id)->status}}</td>
                            </tr>
                        @endforeach
                    @else
                        <p>You have not placed any order yet!</p>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection