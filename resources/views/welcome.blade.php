@extends('layouts.app')
@section('content')
<div class="wrapper">
    <div style="width: 100%">
        <div class="bd-example">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="/images/slider/1.jpg" width="900px" height="300" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2  class="text-white" style="text-shadow: #4e555b;font-size: 3.5rem"><span style="color: pink; font-weight: 700">Bouquets</span> of All Varieties</h2>
                            <p>Find out all the images</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/images/slider/2.jpg" width="900px" height="300" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="text-white" style="text-shadow: #4e555b;font-size: 3.5rem"><span style="color: #761b18; font-weight: 700">Customize</span> Bouquets</h2>
                            <p>You can get a custom bouquet by selecting the flowers you like.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/images/slider/3.jpg" width="900px" height="300" alt="second slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h2 class="text-white" style="text-shadow: #4e555b;font-size: 3.5rem"><span style="color: greenyellow;font-weight: 700">EyeCatching</span> Wrappers</h2>
                            <p>You can get a custom bouquet by selecting the flowers you like.</p>
                        </div>
                    </div>
                    {{--<div class="carousel-item">--}}
                        {{--<img class="d-block w-100" src="/images/slider/4.jpg" width="900px" height="300" alt="Third slide">--}}
                        {{--<div class="carousel-caption d-none d-md-block">--}}
                            {{--<h2 class="text-white" style="text-shadow: #4e555b;font-size: 3.5rem">Customize Bouquets</h2>--}}
                            {{--<p>You can get a custom bouquet by selecting the flowers you like.</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
    <div class=" container-fluid">
    <div class="row mt-4">
        <div class="col-lg-2" style="position:sticky;z-index: 1">
            <div class="card">
                <div class="card-header bg-success text-white">
                    <h5 class="text-center">Categories</h5>
                </div>
                <div class="card-body">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a href="{{route('showAllBouquets')}}" class="btn btn-outline-primary btn-block">
                            All
                        </a>
                        @foreach($categories as $category)
                            <a href="/show/{{$category->id}}" class="btn btn-outline-primary btn-block">
                                {{$category->name}}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-10">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <h2 class="text-center font-weight-bold">{{$categoryName}}</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if(!empty($bouquets))
                        @foreach($bouquets as $bouquet)
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <img class="card-img-top" src="/images/bouquets/{{$bouquet->bouquetImage}}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$bouquet->name}}</h5>
                                        <p class="card-text">{{$bouquet->description}}</p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <form action="{{route('details',$bouquet->id)}}">
                                        <input type="submit"  class="btn btn-primary btn-block" value="View Details"/>
                                    </form>

                                </div>
                            </div>
                            @endforeach
                            @endif
                    </div>
                    <button onclick="topFunction()" id="myBtn" title="Go to top"
                    style="
                          display: none;
                          position: fixed;
                          bottom: 20px;
                          right: 30px;
                          z-index: 99;
                          font-size: 18px;
                          border: none;
                          outline: none;
                          background-color: red;
                          color: white;
                          cursor: pointer;
                          padding: 15px;
                          border-radius: 50%;
                        "
                    >Top</button>




                </div>
            </div>

        </div>

    </div>
    </div>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</div>
    @endsection